const express = require('express')
const bodyParser = require('body-parser')
const loginController = require('../Controller/loginController')
const loginRouter= express.Router()

loginRouter.post('/',loginController.loginController)

module.exports =loginRouter;
