const express = require('express')
const bodyParser = require('body-parser')
const registerController = require('../Controller/registerController')
const registerRouter= express.Router()


registerRouter.post('/',registerController.registerController)

module.exports = registerRouter;
