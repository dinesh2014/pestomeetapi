const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const registerRouter = require('./Routes/registerRoute.js')
const loginRouter = require('./Routes/loginRoute.js')

const app = express();

app.use(bodyParser.json())
app.use(cors({origin:'*'}))

app.use('/api/pesto/register',registerRouter)
app.use('/api/pesto/login',loginRouter)

//app.use(authMiddleware)


const serverListen = app.listen(process.env.PORT||5000,function(){
  console.log('Server Started Successfully')
  })
