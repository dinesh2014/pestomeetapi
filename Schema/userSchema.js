const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  id:{type:mongoose.Mixed , required:true} ,
  name:{type:String , required:true},
  email: {type:mongoose.Mixed , required:true},
  phone:{type:Number , required:true},
  role:{type:String , required:true},
  password:{type:mongoose.Mixed,require:true}
});


module.exports=  mongoose.model("userModel",userSchema);
