require('dotenv').config()
const express = require('express')
const cors = require('cors')
const bodyParser = require("body-parser")
const mongoose = require('mongoose');
const bcrypt =require('bcrypt')
const { v4: uuidv4 } = require('uuid');
var jwt = require('jsonwebtoken');
const registerUser = require('../Schema/userSchema.js')


const app = express()
app.use(bodyParser.json())
app.use(cors()) // Use this after the variable declaration

const users =[];
const uri = "mongodb+srv://"+process.env.DB_USER+":"+process.env.DB_PASS+"@"+process.env.DB_HOST+"/"+process.env.DB_NAME

const loginController =(req,res,next)=>{
const {email,phone,password} = req.body;
console.log(email)
console.log(phone)
console.log(password)
  mongoose.connect(uri,{useNewUrlParser: true,useUnifiedTopology: true})
          .then(() => console.log("connected to DB."))
          .catch( err => console.log(err));


  registerUser.findOne({$or:[{'email':email},{'phone':phone}]},function(err,result){
    if(!err){
      if(!result){
        res.json({message:"User is not registered with us"})
      }else if(bcrypt.compareSync(password, result.password)){
           console.log("JWT Token Created")
           console.log(result)
           var key =process.env.JWT_SECRET
           try{
             jwt.sign({name:result.name , id:result.id }, key,function(err, token) {
               if(err){
                 res.json({error:err})
                 console.log("Error")
               }else{
                 res.json({message:"User Authenticated" ,auth:true,token:token,name:result.name,role:result.role,id:result.id});
                 console.log(token)
               }})
           }catch{
             res.json({message:"Authentication Error"})
           }
      }else if(! bcrypt.compareSync(password, result.password)){
           res.json({message:"Wrong Username/Password"})
      }else{
        res.json({message:"Error Happened while registering User, Try Again !"})
      }
    } else{
        res.json({message:"Error Happened while registering User, Try Again !"})
    }
  })

}

exports.loginController=loginController
