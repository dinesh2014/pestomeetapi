require('dotenv').config()
const express = require('express')
const cors = require('cors')
const bodyParser = require("body-parser")
const mongoose = require('mongoose');
const bcrypt =require('bcrypt')
const { v4: uuidv4 } = require('uuid');
const registerUser = require('../Schema/userSchema.js')
const users =[];
var id = uuidv4();
const uri = "mongodb+srv://"+process.env.DB_USER+":"+process.env.DB_PASS+"@"+process.env.DB_HOST+"/"+process.env.DB_NAME
const registerController =(req,res,next)=>{
console.log(req.body)
const {name,email,phone,password,role} = req.body;
const hash = bcrypt.hashSync(password,10);
let user={"id":id,"name":name,"email":email,"phone":phone,"password":hash,"role":role}

  mongoose.connect(uri,{useNewUrlParser: true,useUnifiedTopology: true})
          .then(() => console.log("connected to DB."))
          .catch( err => console.log(err));

  const userDetail = new registerUser({"id":id,"name":name,"email":email,"phone":phone,"password":hash,"role":role})


  registerUser.findOne({$or:[{'email':email},{'phone':phone}]},function(err,result){
    if(!err){
      if(!result){
          userDetail.save(function(err,result){
                    if (err){res.json({message:err}); }
                    else{res.json({message:"User Registered Successfully"})}})
      }else{
        console.log(result)
        res.json({message:"User Already Available"})
      }
    } else{
        res.json({message:"Error Happened while registering User, Try Again !"})
    }
  })

}

exports.registerController =registerController
